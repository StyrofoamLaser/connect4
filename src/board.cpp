#include "board.h"
#include <iostream>
#include <string>
using namespace std;

Board::Board() 
{
    initBoard();
}

void Board::initBoard() 
{
    for (int i = 0; i < BOARD_HEIGHT; i++) 
    {
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            grid[j][i] = " ";
        }
    }
}

void Board::displayBoard()
{
    cout << "======================" << endl;
    string boardString = "|";

    for (int i = 0; i < BOARD_HEIGHT; i++) 
    {
        for (int j = 0; j < BOARD_WIDTH; j++)
        {
            boardString += grid[j][i] + " |";
        }

        if (i != BOARD_HEIGHT-1) 
        {
            boardString += "\n|";
        }
    }

    cout << boardString << endl;
}

bool Board::insertPiece(string piece, int col) 
{
    col--; // our column is presented in a human friendly format but is actually 1 less 
    // first, check if the column is full
    if (isColumnFull(col)) 
    {
        cout << "That column is already full! Choose another." << endl;
        return false;
    } 

    // next, find the lowest row we can play in

    for (int i = BOARD_HEIGHT-1; i >= 0; i--)
    {
        if (grid[col][i] == " ")
        {
            grid[col][i] = piece;
            return true;
        }
    }

    // something weird happened if we get here.
    cout << endl << "Something weird happened...";
    return false;
}

bool Board::isColumnFull(int col) 
{
    return grid[col][0] != " ";
}

void Board::findChains()
{
    xChains.clear();
    oChains.clear();


    for(int i=0; i < BOARD_HEIGHT; i++)
    {
        for (int j=0; j < BOARD_WIDTH; j++)
        {
            if (grid[j][i] != " ")
            {
                string piece = grid[j][i];
                
                vector<Chain> tempChains;
                
                if (j >= SPACES_TO_WIN) tempChains.push_back(buildChain(i, j, piece, Direction::LEFT));
                if (j < BOARD_WIDTH-1) tempChains.push_back(buildChain(i, j, piece, Direction::RIGHT));
                if (i < BOARD_HEIGHT-1) tempChains.push_back(buildChain(i, j, piece, Direction::UP));
                if (i >= SPACES_TO_WIN) tempChains.push_back(buildChain(i, j, piece, Direction::DOWN));
                if (i < BOARD_HEIGHT-1 && j >= SPACES_TO_WIN) tempChains.push_back(buildChain(i, j, piece, Direction::UP_LEFT));
                if (i < BOARD_HEIGHT-1 && j < BOARD_WIDTH-1) tempChains.push_back(buildChain(i, j, piece, Direction::UP_RIGHT));
                if (i >= SPACES_TO_WIN && j >= SPACES_TO_WIN) tempChains.push_back(buildChain(i, j, piece, Direction::DOWN_LEFT));
                if (i >= SPACES_TO_WIN && j < BOARD_WIDTH-1) tempChains.push_back(buildChain(i, j, piece, Direction::DOWN_RIGHT));

                if (piece == "X")
                {
                    xChains.insert(xChains.end(), tempChains.begin(), tempChains.end());
                }
                else 
                {
                    oChains.insert(oChains.end(), tempChains.begin(), tempChains.end());
                }
            }
        }
    }
}

bool Board::checkForWin(string candidate)
{
    if (candidate == "X")
    {
        return getLongestChainLength(xChains) >= SPACES_TO_WIN;
    }
    else
    {
        return getLongestChainLength(oChains) >= SPACES_TO_WIN;
    }

    return false;
}

Board::Chain Board::buildChain(int row, int col, string piece, Direction dir)
{
    Chain chain;
    chain.startCol = col;
    chain.startRow = row;
    chain.dir = dir;
    chain.length = 0;
    chain.potentialCol = -1;

    int rowMod, colMod, xLimit, yLimit;

    switch (dir)
    {
        case Direction::UP:
            rowMod = 1;
            colMod = 0;
            xLimit = col;
            yLimit = BOARD_HEIGHT;
            break;
        case Direction::DOWN:
            rowMod = -1;
            colMod = 0;
            xLimit = col;
            yLimit = 0;
            break;
        case Direction::LEFT:
            rowMod = 0;
            colMod = -1;
            xLimit = 0;
            yLimit = row;
            break;
        case Direction::RIGHT:
            rowMod = 0;
            colMod = 1;
            xLimit = BOARD_WIDTH;
            yLimit = row;
            break;
        case Direction::UP_LEFT:
            rowMod = 1;
            colMod = -1;
            xLimit = 0;
            yLimit = BOARD_HEIGHT;
            break;
        case Direction::UP_RIGHT:
            rowMod = 1;
            colMod = 1;
            xLimit = BOARD_WIDTH;
            yLimit = BOARD_HEIGHT;
            break;
        case Direction::DOWN_LEFT:
            rowMod = -1;
            colMod = -1;
            xLimit = 0;
            yLimit = 0;
            break;
        case Direction::DOWN_RIGHT:
            rowMod = -1;
            colMod = 1;
            xLimit = BOARD_WIDTH;
            yLimit = 0;
            break;
    }
    
    while (row != yLimit || col != xLimit)
    {
        if (grid[col][row] == piece)
        {
            chain.length++;
            //cout << "Chain length: " << to_string(chain.length) << endl;
            row += rowMod;
            col += colMod;
        }
        else if (grid[col][row] == " ")
        {
            if (col >= 0 && col <= BOARD_WIDTH-1)
            {
                // this spot does not continue our chain, but is open
                chain.potentialCol = col;
            }
            break;
        }
        else 
        {
            // this spot does not continue our chain and is already taken
            break;
        }
    }

    return chain;
}

// checks the given players list of chains for a potential column from the longest chain
int Board::findBestPotential(string candidate)
{
    vector<int> potentials;

    // gather all the valid potential spaces
    if (candidate == "X")
    {
        int longest = getLongestChainLength(xChains);
        if (longest <= 1) return -1; // if our chains are too small, don't bother

        for(Chain c : xChains)
        {
            if (c.length == longest && c.potentialCol >= 0) 
            {
                potentials.push_back(c.potentialCol+1);
            }
        }
    }
    else 
    {
        int longest = getLongestChainLength(oChains);
        if (longest <= 1) return -1;

        for(Chain c : oChains)
        {
            if (c.length == longest && c.potentialCol >= 0) 
            {
                potentials.push_back(c.potentialCol+1);
            }
        }
    }

    //cout << "Num potential spots: " << to_string(potentials.size()) << endl;

    if (potentials.size() == 0) 
    {
        return -1;
    }

    // choose randomly from the possible best choices
    int r = rand() % potentials.size();
    return potentials.at(r);
}

int Board::getLongestChainLength(vector<Chain> chains)
{
    int longest = 0;
    for (Chain c : chains)
    {
        if (c.length > longest)
        {
            longest = c.length;
        }
    }

    return longest;
}