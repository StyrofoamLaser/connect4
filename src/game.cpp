
#include "game.h"
#include <iostream>
#include <stdlib.h>

Game::Game() 
{
    board = Board();

    changeState(GameState::MENU);
}

// private functions
void Game::displayStartMenu() 
{
    cout << endl << "CONNECT FOUR" << endl;
    cout << "=============" << endl;
    cout << "1. Play Game" << endl;
    cout << "2. Quit" << endl;
    cout << "Enter your #:";
    
    int choice;
    cin >> choice;

    switch (choice)
    {
        case 1:
            cout << "YOU ARE PLAYING AS 'X'! YOU GO FIRST!" << endl;
            changeState(GameState::PLAYER_TURN);
            break;
        case 2:
            changeState(GameState::QUIT);
            break;
        default:
            break;
    }
}

void Game::changeState(GameState newState) 
{
    state = newState;

    switch(newState) 
    {
        case GameState::MENU:
            board.initBoard();
            displayStartMenu();
            break;
        case GameState::PLAYER_TURN:
            promptPlayerInput();
            break;
        case GameState::AI_TURN:
            computerTakeTurn();
            break;
        case GameState::QUIT:
            break;
    }
}

void Game::promptPlayerInput() 
{
    board.displayBoard();
    cout << "======================" << endl;

    bool validChoice = false;
    int choice;

    while (!validChoice) 
    {    
        cout << "\nChoose a column (1-7): ";
        cin >> choice;

        validChoice = (choice >= 1 && choice <= 7);
    }

    bool successfulPlay = board.insertPiece("X", choice); // will maybe let the player choose their piece later, for now its X

    if (successfulPlay) 
    { 
        board.findChains();
        if (board.checkForWin("X"))
        {
            // win condition
            board.displayBoard();

            cout << "======================" << endl;
            cout << "4 in a row! Congratulations, you've won!" << endl;
            changeState(GameState::MENU);
        }
        else
        {
            board.displayBoard();
            changeState(GameState::AI_TURN);
        }
    }
    else 
    {
        promptPlayerInput();
    }
}

void Game::computerTakeTurn()
{
    cout << endl << "Computer is thinking..." << endl;
    int colToPlay = compChooseMove();

    board.insertPiece("O", colToPlay);
    board.findChains();

    cout << "Computer plays in column " << to_string(colToPlay) << "." << endl;

    if (board.checkForWin("O"))
    {
        // lose condition
        board.displayBoard();

        cout << "======================" << endl;
        cout << "Uh oh, the computer has 4 in a row! Better luck next time!" << endl;
        changeState(GameState::MENU);
    }
    else 
    {
        changeState(GameState::PLAYER_TURN);
    }
}

int Game::compChooseMove() 
{
    int colChoice;
    // ai priorities are as follows:
    // 1. try to block the player from winning
    colChoice = board.findBestPotential("X");
    if (colChoice != -1) return colChoice;

    // 2. try to progress towards 4-in-a-row of its own piece
    colChoice = board.findBestPotential("O");
    if (colChoice != -1) return colChoice;

    // 3. if it cannot do one of the above, choose a random valid column.
    bool validChoice = false;

    while (!validChoice) 
    {
        colChoice = rand() % BOARD_WIDTH+1;

        validChoice = !board.isColumnFull(colChoice-1);
    }

    return colChoice;
}

// public functions
// initially thought i would need an update loop, but because this game is not real time,
// it doesn't really do much....
void Game::update() 
{
    switch(state)
    {
        case GameState::MENU:
            displayStartMenu();
            break;
    }
}

bool Game::isGameOver() 
{
    return state == GameState::QUIT;
}