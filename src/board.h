
#include <string>
#include <vector>
using namespace std;

const static int BOARD_WIDTH = 7;
const static int BOARD_HEIGHT = 6;
const static int SPACES_TO_WIN = 4;

class Board
{
    private:

    std::string grid[BOARD_WIDTH][BOARD_HEIGHT];

    enum Direction
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        UP_LEFT,
        UP_RIGHT,
        DOWN_LEFT,
        DOWN_RIGHT
    };

    struct Chain
    {
        int startCol;
        int startRow;
        int length;
        int potentialCol;
        Direction dir;
    };
    
    vector<Chain> xChains;
    vector<Chain> oChains;

    public:

    Board();

    void initBoard();
    void displayBoard();
    bool insertPiece(string piece, int col);
    bool isColumnFull(int col);

    void findChains();
    Chain buildChain(int row, int col, string piece, Direction dir);

    int findBestPotential(string candidate);
    int getLongestChainLength(vector<Chain> chains);
    bool checkForWin(string candidate);
};