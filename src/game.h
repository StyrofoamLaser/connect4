#include <iostream>
#include <vector>
using namespace std;

#include "board.h"

class Game 
{
    private:

    enum GameState 
    {
        MENU,
        PLAYER_TURN,
        AI_TURN,
        QUIT
    };

    void displayStartMenu();
    void changeState(GameState newState);

    void promptPlayerInput();

    int compChooseMove();
    void computerTakeTurn();

    void initMoves();

    GameState state;
    Board board;

    public:

    Game();

    void update();
    bool isGameOver();
};