
#include <iostream>
#include "game.h"
using namespace std;

// So, what's required for a vs AI connect 4 game?
// need a 'game state' so we can track player vs computer turns
// need a 7x6 grid of spaces which can hold either 'O' or 'X'
// need to assign the player and computer to a certain letter and report to the player which is theirs
// need to prompt the player the column they wish to play in
// need to display the result on to the board in ascii art
// need to have the computer choose a column and update the board
// need to check the grid for win-conditions
// if a win-condition is met, need to end the game, report the winner, and possibly prompt to play again
// to get a little fancy, maybe keep track of a scoreboard of wins/losses.

int main () {
    Game game = Game();
    
    while (!game.isGameOver()) {
        game.update();
    }

    cout << "\n== Thank you for playing! ==";
    return 0;
}