# Connect-Four!

## Summary
This project is a single-player (vs AI) game of Connect-Four playable via CMD line.

## Build/Run Instructions
You should be able to compile `main.cpp` with your C++ compiler of choice and then run the resulting exe from the command-line.

I personally built using the Visual Studio command-line tools via a `Developer Command Prompt` and using `cl.exe`.
The full command being: `cl /Fe"connect4" src/main.cpp src/board.cpp src/game.cpp`

I've also included a pre-compiled executable (`connect4.exe`) which you can run via CMD line.

## Possible Improvements
Given the time-frame for the test, theres a lot that could be improved in this project given more time. Here are a few possibilities:

1. Could potentially improve the "grading" of potential spots using a pathfinding algorithm.
2. The computer ai could be made much more intelligent using the above system.
3. Could allow the player to choose between 'X' and 'O' pieces, as well as going first or second.
4. Plenty of cleanup both in the code and aesthetically that could be done.
5. Could keep track of wins/losses in a file and maybe "high scores" for how many moves it took you to win.
6. AI difficulty levels?
7. Could find a way to clear the screen so it's easier to follow :)